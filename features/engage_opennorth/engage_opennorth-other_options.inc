<?php

/**
 * @file
 * Form alteration affecting the multifield on the Open North engage
 * registration form.
 */

// Here, we'll add an option to delect a neighbouring representative.
if (isset($form['field_engage_to_field']['und'][0]['email']['#default_value'])) {
  // Other Options Markup.
  if (isset($node->field_engage_alter_email_target['und'][0]['value'])) {
    $collapse_heading = $node->field_engage_alter_email_targets['und'][0]['value'];
  }
  else {
    $collapse_heading = t('Not your rep?');
  }
  $form['other_options'] = array(
    '#type' => 'markup',
    '#markup' => substr(engage_opennorth_collapse_content('', $collapse_heading), 0, -12),
    '#name' => 'other_mp_select',
    '#weight' => 2,
  );
}
