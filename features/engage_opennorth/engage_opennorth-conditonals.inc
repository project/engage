<?php

/**
 * @file
 * Form '#states' for the Open North engage feature registration form.
 */

  // Conditional States can be used to conditionally hide form elements.
  $form['actions']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['field_engage_first_name']['#states'] = array(
    'visible' => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
    'required' => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['field_engage_last_name']['#states'] = array(
    'visible' => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
    'required' => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['field_engage_keep_me_informed']['#states'] = array(
    'visible' => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
    'required' => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['field_engage_postal_code']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('empty' => TRUE),
    ),
  );
  $form['field_engage_to_field']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['field_engage_cc_field']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_cc_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['field_engage_bcc_field']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_bcc_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['field_engage_subject']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['field_engage_message']['und'][0]['#suffix'] = '<h1>' . t('Your info') . '</h1>';
  $form['field_engage_message']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['other_mp_select']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['other_prov_select']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
  $form['other_mun_select']['#states'] = array(
    'visible'  => array(
      ':input[name="field_engage_to_field[und][0][email]"]' => array('filled' => TRUE),
    ),
  );
