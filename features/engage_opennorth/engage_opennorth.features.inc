<?php
/**
 * @file
 * engage_opennorth.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function engage_opennorth_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "multifield" && $api == "") {
    return array("version" => "");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function engage_opennorth_node_info() {
  $items = array(
    'engage_campaign' => array(
      'name' => t('Engage Campaign'),
      'base' => 'node_content',
      'description' => t('Want to provide users with a postal-code based tool to engage with Canadian public officials? This is the content type for you, my friend!'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_registration_type().
 */
function engage_opennorth_default_registration_type() {
  $items = array();
  $items['engage_campaign_registration'] = entity_import('registration_type', '{
    "name" : "engage_campaign_registration",
    "label" : "Engage Campaign Registration",
    "locked" : "0",
    "weight" : "0",
    "data" : null,
    "rdf_mapping" : []
  }');
  return $items;
}
