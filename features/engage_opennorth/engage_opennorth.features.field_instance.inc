<?php
/**
 * @file
 * engage_opennorth.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function engage_opennorth_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'multifield-field_engage_campaign_targets-field_engage_email'
  $field_instances['multifield-field_engage_campaign_targets-field_engage_email'] = array(
    'bundle' => 'field_engage_campaign_targets',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'email',
        'settings' => array(),
        'type' => 'email_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'multifield',
    'field_name' => 'field_engage_email',
    'label' => 'Email',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'multifield-field_engage_campaign_targets-field_engage_recipient_type'
  $field_instances['multifield-field_engage_campaign_targets-field_engage_recipient_type'] = array(
    'bundle' => 'field_engage_campaign_targets',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'multifield',
    'field_name' => 'field_engage_recipient_type',
    'label' => 'Recipient Type',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'multifield-field_engage_campaign_targets-field_engage_target_type'
  $field_instances['multifield-field_engage_campaign_targets-field_engage_target_type'] = array(
    'bundle' => 'field_engage_campaign_targets',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'multifield',
    'field_name' => 'field_engage_target_type',
    'label' => 'Target Type',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-body'
  $field_instances['node-engage_campaign-body'] = array(
    'bundle' => 'engage_campaign',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The node body.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'display_summary' => 1,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-field_engage_alter_email_targets'
  $field_instances['node-engage_campaign-field_engage_alter_email_targets'] = array(
    'bundle' => 'engage_campaign',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The text that will show up to prompt the user to look for an alternative representative from a neighbouring postal code.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_engage_alter_email_targets',
    'label' => 'Alter Email Targets Prompt',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-field_engage_call_to_action'
  $field_instances['node-engage_campaign-field_engage_call_to_action'] = array(
    'bundle' => 'engage_campaign',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Something catchy to inspire users to join the cause.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_engage_call_to_action',
    'label' => 'Call to action',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-field_engage_campaign_targets'
  $field_instances['node-engage_campaign-field_engage_campaign_targets'] = array(
    'bundle' => 'engage_campaign',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Add as many campaign targets as you like - select their level of government or simply enter an email address and address them as To, Cc or Bcc.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_engage_campaign_targets',
    'label' => 'Campaign Targets',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'multifield',
      'settings' => array(),
      'type' => 'multifield_default',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-field_engage_highlighted_image'
  $field_instances['node-engage_campaign-field_engage_highlighted_image'] = array(
    'bundle' => 'engage_campaign',
    'deleted' => 0,
    'description' => 'Optionally, add an image to the campaign.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_engage_highlighted_image',
    'label' => 'Highlighted Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => 'engage_campaign',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-field_engage_message'
  $field_instances['node-engage_campaign-field_engage_message'] = array(
    'bundle' => 'engage_campaign',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This is the text that will eventually be sent to the campaign targets.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_engage_message',
    'label' => 'Message',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-field_engage_participant_goal'
  $field_instances['node-engage_campaign-field_engage_participant_goal'] = array(
    'bundle' => 'engage_campaign',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'How many people are we hoping will support this campaign?',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'engage_progress',
        'settings' => array(
          'engage_progress_settings' => array(
            'count_source' => 'registration',
            'registration_field' => 'field_engage_sign_petition',
          ),
        ),
        'type' => 'engage_progress_bar',
        'weight' => 3,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'engage_progress',
        'settings' => array(
          'engage_progress_settings' => array(
            'count_source' => 'registration',
            'registration_field' => 'field_engage_sign_petition',
          ),
        ),
        'type' => 'engage_progress_bar',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'inline',
        'module' => 'engage_progress',
        'settings' => array(
          'engage_progress_settings' => array(
            'count_source' => 'registration',
            'registration_field' => 'field_engage_sign_petition',
          ),
        ),
        'type' => 'engage_progress_bar',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_engage_participant_goal',
    'label' => 'Participant Goal',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-field_engage_sign_petition'
  $field_instances['node-engage_campaign-field_engage_sign_petition'] = array(
    'bundle' => 'engage_campaign',
    'default_value' => array(
      0 => array(
        'registration_type' => 'engage_campaign_registration',
      ),
    ),
    'deleted' => 0,
    'description' => 'Select the registration type that is associated with this campaign. Unless you\'re extending Engage beyond what it was intended to do out-of-the-box, this should be set to "Engage Campaign Registration".',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'registration',
        'settings' => array(),
        'type' => 'registration_form',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'registration',
        'settings' => array(),
        'type' => 'registration_form',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_engage_sign_petition',
    'label' => 'Sign Petition',
    'required' => 1,
    'settings' => array(
      'default_registration_settings' => array(
        'capacity' => 0,
        'reminder' => array(
          'reminder_settings' => array(
            'reminder_date' => '',
            'reminder_template' => '',
          ),
          'send_reminder' => 0,
        ),
        'scheduling' => array(
          'close' => '',
          'open' => '',
        ),
        'settings' => array(
          'confirmation' => 'Registration has been saved.',
          'from_address' => 'dylan@openconcept.ca',
          'maximum_spaces' => 1,
          'multiple_registrations' => 0,
        ),
        'status' => 1,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'registration',
      'settings' => array(),
      'type' => 'registration_select',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-engage_campaign-field_engage_subject'
  $field_instances['node-engage_campaign-field_engage_subject'] = array(
    'bundle' => 'engage_campaign',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The subject of the email that will be sent to the campaign targets.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_engage_subject',
    'label' => 'Subject',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_bcc_field'
  $field_instances['registration-engage_campaign_registration-field_engage_bcc_field'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'email',
        'settings' => array(),
        'type' => 'email_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_bcc_field',
    'label' => 'Bcc',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_cc_field'
  $field_instances['registration-engage_campaign_registration-field_engage_cc_field'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'email',
        'settings' => array(),
        'type' => 'email_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_cc_field',
    'label' => 'Cc',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_first_name'
  $field_instances['registration-engage_campaign_registration-field_engage_first_name'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_first_name',
    'label' => 'First Name',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_keep_me_informed'
  $field_instances['registration-engage_campaign_registration-field_engage_keep_me_informed'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_keep_me_informed',
    'label' => 'Keep me informed about this campaign',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_last_name'
  $field_instances['registration-engage_campaign_registration-field_engage_last_name'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_last_name',
    'label' => 'Last Name',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_message'
  $field_instances['registration-engage_campaign_registration-field_engage_message'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_message',
    'label' => 'Message',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_postal_code'
  $field_instances['registration-engage_campaign_registration-field_engage_postal_code'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_postal_code',
    'label' => 'Postal Code',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 10,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_subject'
  $field_instances['registration-engage_campaign_registration-field_engage_subject'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_subject',
    'label' => 'Subject',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'registration-engage_campaign_registration-field_engage_to_field'
  $field_instances['registration-engage_campaign_registration-field_engage_to_field'] = array(
    'bundle' => 'engage_campaign_registration',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'email',
        'settings' => array(),
        'type' => 'email_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'registration',
    'field_name' => 'field_engage_to_field',
    'label' => 'To',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add as many campaign targets as you like - select their level of government or simply enter an email address and address them as To, Cc or Bcc.');
  t('Alter Email Targets Prompt');
  t('Bcc');
  t('Body');
  t('Call to action');
  t('Campaign Targets');
  t('Cc');
  t('Email');
  t('First Name');
  t('Highlighted Image');
  t('How many people are we hoping will support this campaign?');
  t('Keep me informed about this campaign');
  t('Last Name');
  t('Message');
  t('Optionally, add an image to the campaign.');
  t('Participant Goal');
  t('Postal Code');
  t('Recipient Type');
  t('Select the registration type that is associated with this campaign. Unless you\'re extending Engage beyond what it was intended to do out-of-the-box, this should be set to "Engage Campaign Registration".');
  t('Sign Petition');
  t('Something catchy to inspire users to join the cause.');
  t('Subject');
  t('Target Type');
  t('The node body.');
  t('The subject of the email that will be sent to the campaign targets.');
  t('The text that will show up to prompt the user to look for an alternative representative from a neighbouring postal code.');
  t('This is the text that will eventually be sent to the campaign targets.');
  t('To');

  return $field_instances;
}
