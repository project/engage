<?php
/**
 * @file
 * Template file for the theming an engage progress bar.
 *
 * Available custom variables:
 * - $item: An associative array containing the value and count for the field.
 *     - count: The current progress count.
 *     - value: The value configured in the number_integer field. It represents
 *       the goal.
 * - $field_name: The number_integer field we are themeing.
 * - $entity_type: The entity type the field is attached to.
 * - $bundle: The bundle of the entity the field is attached to.
 * - $langcode: The language code as passed to the field_formatter_view hook.
 * - $goal: The goal for the progress field.
 * - $count: The current progress count.
 * - $percent: The percentage towards reaching the goal.
 * - $accessible_msg: A message to display or at least have available to screen
 *     readers that provides information about the progress.
 *
 * This themplate can be overridden with the following template suggestions in
 * the order listed:
 *    engage_progress_progress_bar_<entity_type>_<bundle>_<field_name>
 *    engage_progress_progress_bar_<entity_type>_<field_name>
 *    engage_progress_progress_bar_<field_name>
 *    engage_progress_progress_bar_<entity_type>_<bundle>
 *    engage_progress_progress_bar_<entity_type>
 *    engage_progress_progress_bar
 *
 * @see engage_progress_theme().
 * @see theme_engage_progress().
 */
?>
<div class="engage-progress-message"><span class="engage-progress-count"><?php print $count; ?></span> <?php print t('Signed of @goal', array('@goal' => $goal)); ?></div>
<div class="engage-progress-bar">
  <div class="engage-progress-bar-complete"  style="width: <?php print $percent?>%"><div class='element-invisible'><?php print $accessible_msg; ?></div></div>
</div>
