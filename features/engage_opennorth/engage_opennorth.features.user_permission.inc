<?php
/**
 * @file
 * engage_opennorth.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function engage_opennorth_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create engage_campaign_registration registration'.
  $permissions['create engage_campaign_registration registration'] = array(
    'name' => 'create engage_campaign_registration registration',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  // Exported permission: 'create engage_campaign_registration registration other anonymous'.
  $permissions['create engage_campaign_registration registration other anonymous'] = array(
    'name' => 'create engage_campaign_registration registration other anonymous',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'registration',
  );

  return $permissions;
}
