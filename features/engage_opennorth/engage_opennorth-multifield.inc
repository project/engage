<?php

/**
 * @file
 * Form alteration affecting the multifield on the Open North engage
 * registration form.
 */

/**
 * Using hook_form_alter.
 *
 * This form_alter simply adjusts the multifield form on the campaign's edit
 * page. Just overriding the select list labels.
 */
function engage_opennorth_form_campaign_node_form_alter(&$form, &$form_state, $form_id) {
  foreach ($form['field_engage_campaign_targets']['und'] as $key => $target) {
    if (is_array($target) && isset($target['field_engage__target_type'])) {
      $form['field_engage_campaign_targets']['und'][$key]['field_engage_target_type']['und']['#options']['_none'] = $form['field_engage_campaign_targets']['und'][$key]['field_engage_target_type']['und']['#title'];
      unset($form['field_engage_campaign_targets']['und'][$key]['field_engage_target_type']['und']['#title']);
      $form['field_engage_campaign_targets']['und'][$key]['field_engage_recipient_type']['und']['#options']['_none'] = $form['field_engage_campaign_targets']['und'][$key]['field_engage_recipient_type']['und']['#title'];
      unset($form['field_engage_campaign_targets']['und'][$key]['field_engage_recipient_type']['und']['#title']);
      $form['field_engage_campaign_targets']['und'][$key]['field_engage_email']['und'][0]['email']['#attributes']['placeholder'] = $form['field_engage_campaign_targets']['und'][$key]['field_engage_email']['und'][0]['email']['#title'];
      unset($form['field_engage_campaign_targets']['und'][$key]['field_engage_email']['und'][0]['email']['#title']);
    }
  }
}
