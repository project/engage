<?php

/**
 * @file
 * Ajaxiness for the engage_opennorth.
 */

/**
 * AJAX callback for the Engage registration form.
 */
function engage_opennorth_ajax_callback(&$form, &$form_state) {

  if (isset($form['field_engage_to_field']['und'][0]['email']['#default_value'])) {
    $form['find_mp']['#attributes'] = array(
      'class' => array(
        'update-button',
      ),
      'id' => 'edit-find-mp',
    );
    $form['find_mp']['#value'] = t('Update my representatives');
    $form['find_mp']['#weight'] = 4;
    $form['find_mp']['#suffix'] = '</div></div>';
  }
  $node = node_load($form['#entity']->entity_id);
  $form['field_engage_subject']['und'][0]['value']['#value'] = $node->field_engage_subject['und'][0]['value'];
  $form['field_engage_message']['und'][0]['value']['#value'] = $node->field_engage_message['und'][0]['value'];

  foreach ($form['field_engage_to_field']['und'] as $key => $to_item) {
    if (is_array($to_item)) {
      if (isset($to_item['email'])) {
        $form['field_engage_to_field']['und'][$key]['email']['#prefix'] = '<div class="email">' . $form['field_engage_to_field']['und'][$key]['email']['#value'] . '</div><div class="element-invisible">';
        $form['field_engage_to_field']['und'][$key]['email']['#suffix'] = '</div>';
      }
    }
  }
  foreach ($form['field_engage_cc_field']['und'] as $key => $to_item) {
    if (is_array($to_item)) {
      if (isset($to_item['email'])) {
        $form['field_engage_cc_field']['und'][$key]['email']['#prefix'] = '<div class="email">' . $form['field_engage_cc_field']['und'][$key]['email']['#value'] . '</div><div class="element-invisible">';
        $form['field_engage_cc_field']['und'][$key]['email']['#suffix'] = '</div>';
      }
    }
  }
  foreach ($form['field_engage_bcc_field']['und'] as $key => $to_item) {
    if (is_array($to_item)) {
      if (isset($to_item['email'])) {
        $form['field_engage_bcc_field']['und'][$key]['email']['#prefix'] = '<div class="email">' . $form['field_engage_bcc_field']['und'][$key]['email']['#value'] . '</div><div class="element-invisible">';
        $form['field_engage_bcc_field']['und'][$key]['email']['#suffix'] = '</div>';
      }
    }
  }

  $form['field_engage_to_field']['und']['add_more']['#access'] = 0;
  $form['field_engage_cc_field']['und']['add_more']['#access'] = 0;
  $form['field_engage_bcc_field']['und']['add_more']['#access'] = 0;
  $form['field_engage_to_field']['#weight'] = -1;
  $form['field_engage_cc_field']['#weight'] = 0;
  $form['field_engage_bcc_field']['#weight'] = 1;
  $form['anon_mail']['#prefix'] = '';
  $form['anon_mail']['#suffix'] = '';
  return $form;
}
