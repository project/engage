<?php
/**
 * @file
 * engage_opennorth..inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function engage_opennorth_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_engage_campaign_targets';
  $multifield->label = 'field_engage_campaign_targets';
  $multifield->description = '';
  $export['field_engage_campaign_targets'] = $multifield;

  return $export;
}
